# Inspiration
https://www.lingscars.com/

# What
This is a reusable suite of React components meant to knock up the modern 
website game. Mean to take inspiration from over-the-top, garish, and carnival
esque react components. 

# Includes
* Spinner - A component that spins
* Rocker - A component that rocks back and forther
* Striper - A component that strips back and forth
* NeonSign - A flashing box with with flashiing text. Can control visuals of each letter
* Shaker - A component that shakes
* Jumper - A component that Jumpes on hover
* BoogieMatrix - A multi dimensional array of of images that get auto scaled over eachother and boogey!
* GaudyTitle - A Nice gaudy 
* GaudyParagraph - A Nice gaudy paragraph